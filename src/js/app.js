import axios from 'axios';
import Vue from 'vue';
import Doomino from './vue/Doomino.vue';

const app = new Vue({
    el: '#app',
    components: {
        Doomino
    },
});