# Doomino - Web Client

Ce projet est une interface graphique Web pour le serveur de jeu [Doomino](https://gitlab.com/team-parrot/doomino).

Ce projet a été réalisé par la Team Parrot (H4304), 4IF 2017, INSA Lyon.
Membres de la Team Parrot: *Jean Debard, Thomas Lacroix, Vincent Guillon, Renaud Meurisse, Tom Bourret, Piotr Czajkowski, Kévin Dumanoir.*

## Technologies utilisées

- Node.js
- Laravel Mix
- Vue.js
- axios (Requêtes AJAX via Promises)
- Bootstrap v4
- ECMAScript 6

## Compiler le client

Afin de pouvoir lancer le client pour la première fois, **vous devez installer tout d'abord Node.js et NPM**.

Ensuite, lancez la suite de commande suivante:
```bash
npm install
npm run production
```

Ouvrez ensuite le fichier `index.html`, souriez, tout fonctionne. Précisez l'adresse du serveur de jeu, par défaut: `http://localhost:5001`

## Licence

La totalité de ce répertoire est sous licence GNU 2.0.